from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token
# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=25)
    count = models.IntegerField()
    price = models.FloatField(max_length=5)
    description = models.TextField()

    def __str__(self):
        return self.name


class Order(models.Model):
    product = models.ManyToManyField(Product)
    total_price = models.FloatField()
    buyer = models.ForeignKey(User, related_name='orders')
    date = models.DateTimeField(auto_now_add=True)
    is_paid = models.BooleanField(default=False)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    Signal for create token for created user
    """
    if created:
        Token.objects.create(user=instance)
