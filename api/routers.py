from rest_framework import routers

from api import views


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet, base_name='users')
router.register(r'products', views.ProductViewSet, base_name='products')
router.register(r'orders', views.OrderViewSet, base_name='orders')
