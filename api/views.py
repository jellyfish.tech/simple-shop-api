from django.contrib.auth.models import User

from rest_framework import viewsets, permissions

from api import serializers, models


class UserViewSet(viewsets.ModelViewSet):
    """
    ViewSet for users
    """
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class ProductViewSet(viewsets.ModelViewSet):
    """
    ViewSet for products
    """
    queryset = models.Product.objects.all()
    serializer_class = serializers.ProductSerializer


class OrderViewSet(viewsets.ModelViewSet):
    """
    ViewSet for products
    """
    queryset = models.Order.objects.all()
    serializer_class = serializers.OrderSerializer
