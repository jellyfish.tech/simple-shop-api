from rest_framework import serializers

from api.models import Product, Order, User


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for users
    """

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'url',
            'password'
        ]
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """
        Override method for create new user with hashed password
        :param validated_data: validated data
        :return: new user
        """
        user = User.objects.create_user(**validated_data)
        return user


class ProductSerializer(serializers.ModelSerializer):
    """
    Serializer for products
    """

    class Meta:
        model = Product
        fields = ['id', 'name', 'price', 'count', 'description']


class OrderSerializer(serializers.ModelSerializer):
    """
    Serializer for orders
    """
    buyer = serializers.PrimaryKeyRelatedField(default=serializers.CurrentUserDefault(), queryset=User.objects.all())

    class Meta:
        model = Order
        fields = ['buyer', 'product', 'date', 'total_price', 'is_paid']
